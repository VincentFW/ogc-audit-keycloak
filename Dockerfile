FROM quay.io/keycloak/keycloak:latest as builder

RUN echo "Show-config before build"
RUN /opt/keycloak/bin/kc.sh show-config

ENV KC_METRICS_ENABLED=true
ENV KC_HEALTH_ENABLED=true
# ENV KC_TRANSACTION_XA_ENABLED=false

RUN /opt/keycloak/bin/kc.sh build

RUN echo "Show-config after build"
RUN /opt/keycloak/bin/kc.sh show-config

FROM quay.io/keycloak/keycloak:latest
COPY --from=builder /opt/keycloak/lib/quarkus/ /opt/keycloak/lib/quarkus/
WORKDIR /opt/keycloak
RUN keytool -genkeypair -storepass password -storetype PKCS12 -keyalg RSA -keysize 2048 -dname "CN=server" -alias server -ext "SAN:c=DNS:localhost,IP:127.0.0.1" -keystore conf/server.keystore
ENV KEYCLOAK_ADMIN=admin
ENV KEYCLOAK_ADMIN_PASSWORD=change_me
ENV KC_DB=postgres
ENV KC_DB_URL=jdbc:postgresql://qways-keycloak-dev.postgres.database.azure.com:5432/postgres?&sslmode=require
ENV KC_DB_USERNAME=keycloak
ENV KC_DB_PASSWORD=P6uom6SEYLCeYXtAe4ThnsxQnHZG27
# ENV KC_HOSTNAME=localhost
ENV KC_HTTP_ENABLED=true
ENV KC_HOSTNAME_STRICT=false
ENV QUARKUS_TRANSACTION_MANAGER_ENABLE_RECOVERY=true
EXPOSE 8443

RUN echo "Show-config before ending"
RUN /opt/keycloak/bin/kc.sh show-config

ENTRYPOINT ["/opt/keycloak/bin/kc.sh","start"]